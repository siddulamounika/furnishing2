// google map  
var placeSearch, autocomplete, geolocation;


function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
        {types: ['geocode']});
        
    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    //autocomplete.addListener('place_changed', fillInAddress);
}
  
function geolocate() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        geolocation = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        //console.log('geo'+geolocation.lat);
        var circle = new google.maps.Circle({
          center: geolocation,
          radius: position.coords.accuracy
        });
        autocomplete.setBounds(circle.getBounds());
      });
    }
}

jQuery(document).ready(function() {
	jQuery("#contactnow").on('click', function(event){
		event.preventDefault();

		var url = 'https://api.zinetgo.com/contactUs';
		//var url = 'http://192.168.0.104:8088/contactUs';
		var data ={
			mobile: jQuery("#mobile").val(),
			name: jQuery("#fname").val(),
			email: jQuery("#email").val(),
			message: jQuery("#description").val()
		};
		if(isValidEmailAddress(data.email) && isPhonenumber(data.mobile)){
			jQuery.ajax({
					url: url,
					type:'POST',
					data: JSON.stringify(data),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(data){
						toastr.options.closeButton =true;
						toastr.clear();
						toastr.success('We confirm your request with Furnishing Forum. Our customer care will reach out to you shortly.');
						jQuery('#contact-form').trigger('reset');
					},
					error: function(data){
						toastr.options.closeButton =true;
						toastr.clear();
						toastr.error(data.responseJSON.error);
					}
				});
		}else{
			toastr.clear();
			toastr.error('Invalid Email Id or Wrong Mobile Number');
		}
	});

	jQuery("#bookService").on('click', function(event){
		event.preventDefault();

		var url = 'https://api.zinetgo.com/lead';
		var data ={
			mobile: jQuery("#bmobile").val(),
			name: jQuery("#bname").val(),
			email: jQuery("#bemail").val(),
			address: jQuery("#autocomplete").val(),
			description: ($("#desc").val())? $("#desc").val() : null,
			itemId: $("#serviceId").val()

		};
		if(isValidEmailAddress(data.email) && isPhonenumber(data.mobile)){
			jQuery.ajax({
					url: url,
					type:'POST',
					data: JSON.stringify(data),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(data){
						toastr.options.closeButton =true;
						toastr.clear();
						toastr.success('We confirm your request with Furnishing Forum. Our customer care will reach out to you shortly. For any queries please contact us.');
						jQuery('#booking-form').trigger('reset');
					},
					error: function(data){
						toastr.options.closeButton =true;
						toastr.clear();
						toastr.error(data.responseJSON.error);
					}
				});
		}else{
			toastr.clear();
			toastr.error('Invalid Email Id or Wrong Mobile Number');
		}
	});
});

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

function isPhonenumber(mobile)  {  
  var phoneno = /^\d{10}$/;  
  return phoneno.test(mobile);
} 
	
// $('.moreinfo').hide();
// 	$('.more').click(function (ev) {
// 		var t = ev.target
//    $('#info' + $(this).attr('target')).toggle(500, function(){
//       $(t).html($(this).is(':visible')? 'Jayanagar' : 'Jayanagar ... Click for more location')
//    });
//    return false;
// });

