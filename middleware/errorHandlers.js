exports.notFound = function notFound(req, res, next){
	
	res.status(404).render('404', {
		layout: 'layout',
		title: 'Wrong Turn',
		description: '404 Page Not Found',
		keywords: '404',
		canonical: '',
		city:'bangalore',
		location:'bangalore',
		productURL:null,
		product:null
	});
};

exports.error = function error(err, req, res, next){
	console.log(err);
	res.status(500).render('500', {
		layout: 'layout',
		title: 'Wrong Turn',
		description: '500 internal server error',
		keywords: '500',
		canonical: '', 
		city:'bangalore',
		location:'bangalore',
		productURL:null,
		product:null
	});
};