var  env = require('../env'),
    config = require('../config/' + env.name),
    Client = require('node-rest-client').Client,
    client = new Client(),
    moment = require('moment');

module.exports.index = index;
module.exports.contact = contact;
module.exports.blog = blog;
module.exports.blogDetail = blogDetail;
module.exports.about = about;
module.exports.product = product;
module.exports.service = service;
module.exports.privacy = privacy;

module.exports.bedLinen = bedLinen;
module.exports.blinds = blinds;
module.exports.curtains = curtains;
module.exports.ssofa = ssofa;
module.exports.csofa = csofa;

module.exports.mattress = mattress;
module.exports.doorMats = doorMats;
module.exports.upholstery = upholstery;
module.exports.wallPaper = wallPaper;
module.exports.woodFlooring = woodFlooring;
module.exports.drapingRods = drapingRods;
module.exports.carpets = carpets;

module.exports.onWheel = onWheel;
module.exports.interior = interior;
module.exports.turnkey =turnkey;
//module.exports.productLocation = productLocation;


function index(req,res){
	var meCallback = function(error, data){
		if(data && data.error == undefined){
			res.render('index', {
				layout: 'layout',
				title: 'Home',
				description: 'Zinetgo',
				keywords: 'Zinetgo',
				canonical: '/',
				city:'bangalore',
				location:'bangalore',
				productURL:null,
				product:null,
				moment : moment,
				blogs :  data.data
			});
		}else{
			res.render('500',{
				layout: 'layout',
				title: '500 server error',
				description: 'Zinetgo',
				keywords: 'Zinetgo',
				productURL:null,
				product:null
			});
		}
	}
	var args = {
	  headers: { "Content-Type": "application/json", "Accepts":"application/json"}
	};
	var url =  config.api.url + "/api/blogs"+"?apikey=" + config.api.key + '&psize=3"+"&pno=1';
	var req = client.get(url, args, function (data, response) {
		//console.log(JSON.stringify(data));
		if (data && data.error == undefined){
			meCallback(null, data);
		}else{
			meCallback(data.error, null);
		}
	});
};
function contact(req,res){
	res.render('contact', {
		layout: 'layout',
		title: 'Contact',
		description: 'Zinetgo',
		keywords: 'Zinetgo',
		canonical: '/contact',
		city:'bangalore',
		location:'bangalore',
		productURL:null,
		product:null
	});
};
function blog(req,res){
	var meCallback = function(error, data){
		if(data && data.error == undefined){
			res.render('blog', {
				layout: 'layout',
				title: 'Blogs',
				description: 'Zinetgo',
				keywords: 'blog, Zinetgo',
				canonical: '/blog',
				city:'bangalore',
				location:'bangalore',
				productURL:null,
				product:null,
				moment : moment,
				blogs: data.data
			});
		}else {
			res.render('500',{
				layout: 'layout',
				title: '500 server error',
				description: 'Zinetgo',
				keywords: 'Zinetgo',
				productURL:null,
				product:null
			});
		}
	}
	var args = {
	  headers: { "Content-Type": "application/json", "Accepts":"application/json"}
	};
	var url =  config.api.url + "/api/blogs"+"?apikey=" + config.api.key + '&psize=3"+"&pno=1';
	var req = client.get(url, args, function (data, response) {
		//console.log(JSON.stringify(data));
		if (data && data.error == undefined){
			meCallback(null, data);
		}else{
			meCallback(data.error, null);
		}
	});	
};
function blogDetail(req,res){
	var uname = req.params.uname;
	var meCallback = function(error, data) {
		if(!error) {
			res.render('blog-detail', {
				layout: 'layout',
				title: 'Blogs',
				description: 'Zinetgo',
				keywords: 'blog, Zinetgo',
				canonical: '/blog-detail',
				city:'bangalore',
				location:'bangalore',
				productURL:null,
				product:null,
				moment : moment,
				blog: data.data
			});
		}else {
			res.render('500',{
				layout: 'layout',
				title: '500 server error',
				description: 'Zinetgo',
				keywords: 'Zinetgo',
				productURL:null,
				product:null
			});
		}
	}
	var args = {
	  headers: { "Content-Type": "application/json", "Accepts":"application/json"}
	};
   	var url =  config.api.url+ "/api/blog/"+ uname +"?apikey=" + config.api.key;
    var req = client.get(url, args, function (data, response) {
        if (data && data.error == undefined){
            meCallback(null, data);
        }else{
            meCallback(data.error, null);
        }
    });		
};
function about(req,res){
	res.render('about', {
		layout: 'layout',
		title: 'About Us',
		description: 'Zinetgo',
		keywords: 'about, Zinetgo',
		canonical: '/about',
		city:'bangalore',
		location:'bangalore',
		productURL:null,
		product:null
	});
};
function service(req,res){
	res.render('service', {
		layout: 'layout',
		title: 'Services',
		description: 'Zinetgo',
		keywords: 'Services, Zinetgo',
		canonical: '/service',
		city:'bangalore',
		location:'bangalore',
		productURL:null,
		product:null
	});
};

function privacy(req,res){
	res.render('privacy', {
		layout: 'layout',
		title: 'Privacy',
		description: 'Zinetgo',
		keywords: 'Privacy, Zinetgo',
		canonical: '/privacy',
		city:'bangalore',
		location:'bangalore',
		productURL:null,
		product:null
	});
};

function product(req,res){
	res.render('products', {
		layout: 'layout',
		title: 'Our Products',
		description: 'Zinetgo',
		keywords: 'Products, Zinetgo',
		canonical: '/products',
		city:'bangalore',
		location:'bangalore',
		productURL:null,
		product:null
	});
};

function curtains(req,res){
	res.render('product/curtains', {
		layout: 'layout',
		title: 'Curtains',
		description: 'Zinetgo',
		keywords: 'Curtains ,Zinetgo',
		canonical: '/product/curtains',
		city:'bangalore',
		location:'bangalore',
		productURL:'curtains',
		product:'Curtains'
	});
};

function ssofa(req,res){
	res.render('product/sofa', {
		layout: 'layout',
		title: 'Stanley Sofa',
		description: 'Zinetgo',
		keywords: 'Stanley Sofa, Zinetgo',
		canonical: '/product/stanley-sofa',
		city:'bangalore',
		location:'bangalore',
		productURL:'stanley-sofa',
		product:'Stanley Sofa'
	});
};

function csofa(req,res){
	res.render('product/custom-sofa', {
		layout: 'layout',
		title: 'Custom Sofa',
		description: 'Zinetgo',
		keywords: 'Custom Sofa, Zinetgo',
		canonical: '/product/sofa',
		city:'bangalore',
		location:'bangalore',
		productURL:'sofa',
		product:'Sofa'
	});
};

function mattress(req,res){
	res.render('product/mattress', {
		layout: 'layout',
		title: 'Mattress',
		description: 'Zinetgo',
		keywords: 'Mattress, Zinetgo',
		canonical: '/product/mattress',
		city:'bangalore',
		location:'bangalore',
		productURL:'mattress',
		product:'Mattress'
	});
};

function bedLinen(req,res){
	res.render('product/bed-linen', {
		layout: 'layout',
		title: 'Bed Linen',
		description: 'Zinetgo',
		keywords: 'Bed Linen, Zinetgo',
		canonical: '/product/bed-linen',
		city:'bangalore',
		location:'bangalore',
		productURL:'bed-linen',
		product: 'Bed Linen'
	});
};

function blinds(req,res){
	res.render('product/blinds', {
		layout: 'layout',
		title: 'Blinds',
		description: 'Zinetgo',
		keywords: 'Blinds, Zinetgo',
		canonical: '/product/blinds',
		city:'bangalore',
		location:'bangalore',
		productURL:'blinds',
		product: 'Blinds'
	});
};

function doorMats(req,res){
	res.render('product/door-mats', {
		layout: 'layout',
		title: 'Door Mats',
		description: 'Zinetgo',
		keywords: 'Door Mats, Zinetgo',
		canonical: '/product/door-mats',
		city:'bangalore',
		location:'bangalore',
		productURL:'door-mats',
		product:'Door Mats'
	});
};

function upholstery(req,res){
	res.render('product/upholstery', {
		layout: 'layout',
		title: 'Upholstery',
		description: 'Zinetgo',
		keywords: 'Upholstery, Zinetgo',
		canonical: '/product/upholstery',
		city:'bangalore',
		location:'bangalore',
		productURL:'upholstery',
		product:'Upholstery'
	});
};

function wallPaper(req,res){
	res.render('product/wallpaper', {
		layout: 'layout',
		title: 'Wall Paper',
		description: 'Zinetgo',
		keywords: 'Wall Paper, Zinetgo',
		canonical: '/product/wall-paper',
		city:'bangalore',
		location:'bangalore',
		productURL:'wall-paper',
		product:'Wallpaper'
	});
};

function woodFlooring(req,res){
	res.render('product/wood-flooring', {
		layout: 'layout',
		title: 'Wooden Flooring',
		description: 'Zinetgo',
		keywords: 'Wood Flooring, Zinetgo',
		canonical: '/product/wood-flooring',
		city:'bangalore',
		location:'bangalore',
		productURL:'wood-flooring',
		product:'Wood Flooring'
	});
};

function carpets(req,res){
	res.render('product/carpets', {
		layout: 'layout',
		title: 'Carpets',
		description: 'Zinetgo',
		keywords: 'Carpets, Zinetgo',
		canonical: '/product/carpets',
		city:'bangalore',
		location:'bangalore',
		productURL:'carpets',
		product:'Carpets'
	});
};

function drapingRods(req,res){
	res.render('product/draping-rods', {
		layout: 'layout',
		title: 'Draping Rods',
		description: 'Zinetgo',
		keywords: 'Draping Rods, Zinetgo',
		canonical: '/product/draping-rods',
		city:'bangalore',
		location:'bangalore',
		productURL:'draping-rods',
		product:'Draping Rods'
	});
};

function onWheel(req,res){
	res.render('product/zinetgo-on-wheel', {
		layout: 'layout',
		title: 'zinetgo On Wheel',
		description: 'Zinetgo',
		keywords: 'Zinetgo On Wheel ,Zinetgo',
		canonical: '/product/zinetgo-on-wheel',
		city:'bangalore',
		location:'bangalore',
		productURL:'zinetgo-on-wheel',
		product:'Zinetgo On Wheel'
	});
};

function interior(req,res){
	res.render('product/interior', {
		layout: 'layout',
		title: 'Interior Design',
		description: 'Zinetgo',
		keywords: 'Interior Design, Zinetgo',
		canonical: '/product/interior-design',
		city:'bangalore',
		location:'bangalore',
		productURL:'interior-design',
		product:'Interior Design'
	});
};

function turnkey(req,res){
	res.render('product/turnkey', {
		layout: 'layout',
		title: 'Turnkey Project',
		description: 'Zinetgo',
		keywords: 'Turnkey Project, Zinetgo',
		canonical: '/product/turnkey-project',
		city:'bangalore',
		location:'bangalore',
		productURL:'turnkey-project',
		product:'Turnkey Project'
	});
};

//product/product location
// function productLocation(req, res){
// 	var product = req.params.product;
// 	var locations = ['jayanagar-bangalore','kanakapura-road-bangalore','bannerghatta-road-bangalore','uttarahalli-road-bangalore','jp-nagar-bangalore','rajarajeshwari-nagar-bangalore','banashankari-bangalore'];
// 	var products = ['curtains','carpets','bed-linen','blinds','sofa','door-mates','draping-rods','furnishing-on-wheel','interior-design','mattress','stanley-sofa','turnkey-project','upholstery','wallpaper','wood-flooring'];
// 	var pViewPath = ['product/curtains','product/carpets','product/bed-linen','product/blinds','product/custom-sofa','product/door-mates','product/draping-rods','product/furnishing-on-wheel','product/interior','product/mattress','product/sofa','product/turnkey','product/upholstery','product/wallpaper','product/wood-flooring'];
// 	//var keywords = ['products/painting/interior','products/painting/exterior','products/painting/exterior-texture','products/painting/decorative','products/painting/kids-room','products/painting/waterproofing','products/cleaning/basic','products/cleaning/deep','products/cleaning/move-inout','products/cleaning/sofa','products/cleaning/sump','products/cleaning/carpet','products/cleaning/kitchen','products/cleaning/bathroom','products/polishing/melamine','products/polishing/pu-thinner','products/polishing/pu-water','products/polishing/empurium','products/polishing/mrf','products/polishing/duco','products/polishing/polycote','products/polishing/teak-wood','products/pest-control/ant','products/pest-control/bed-bug','products/pest-control/bee','products/pest-control/cockroach','products/pest-control/mosquito','products/pest-control/rodent','products/pest-control/termite','products/pest-control/wood-borer','products/cleaning/exterior','products/cleaning/floor-polishing'];
// 	if(product){
// 		var data = product.split('-in-');

// 		if(data && data.length == 2 && products.includes(data[0]) && locations.includes(data[1])){
// 			//console.log(locations.includes(data[1]));
// 			var pName = products[products.indexOf(data[0])].replace("-"," ");
// 			pName = pName.toLowerCase().split(' ').map(i => i[0].toUpperCase() + i.substring(1)).join(' ');
// 			pName = pName + 'product';

// 			res.render(pViewPath[products.indexOf(data[0])], {
// 				layout: 'layout', 
// 				canonical: data[0] +'-in-bangalore',
// 				city:'bangalore',
// 				location:data[1],
// 				productURL:data[0],
// 				product: pName,
// 				title: pName,
// 				description:'', 
// 				keywords:''
// 			});
// 		}else{
// 			res.status(404).render('404', {layout: 'layout', menu: '', title: 'Wrong Turn',canonical: '', city:'bangalore',
// 				location:'bangalore',productURL:null,product:null
// 			});
// 		}
// 	}else{
// 		res.status(404).render('404', {layout: 'layout', menu: '', title: 'Wrong Turn',canonical: '', city:'bangalore',
// 				location:'bangalore',productURL:null,product:null
// 		});
// 	}
// };
