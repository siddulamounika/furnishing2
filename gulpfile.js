var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var uglify = require('gulp-uglify');
var cssnano = require('gulp-cssnano');
var minifyCSS = require('gulp-minify-css');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var browserSync = require('browser-sync').create();
var autoprefix = require('gulp-autoprefixer');
var minify = require('gulp-minify');

var DIST = 'dist/';
var DIST_JS = DIST + 'templates/furnishing-template2/assets/js/';
var DIST_CSS = DIST + 'templates/furnishing-template2/assets/css/';
var DIST_IMG = DIST + 'templates/furnishing-template2/assets/img/';
var DIST_FONTS = DIST + 'templates/furnishing-template2/assets/fonts/';
var DIST_FAVICON = DIST + 'templates/furnishing-template2/assets/favicon/';

gulp.task('default', ['js','css','fonts','img','favicon','robots','sitemap'], function(){
    console.log('done!')
})

gulp.task('js', function(){
    return gulp.src([
    'dist/templates/furnishing-template2/assets/js/vendor/jquery-3.1.1.min.js',
    'dist/templates/furnishing-template2/assets/js/vendor/modernizr-2.8.3.min.js',    
    'dist/templates/furnishing-template2/assets/js/plugins.js',
    'dist/templates/furnishing-template2/assets/js/bootstrap.min.js',
    'dist/templates/furnishing-template2/assets/js/toastr.min.js',
    'dist/templates/furnishing-template2/assets/js/main.js',
    'dist/templates/furnishing-template2/assets/js/contact.js'
    ])
    .pipe(uglify())
    .pipe(concat('ff.js'))
    .pipe(gulp.dest(DIST_JS))
});

gulp.task('css', function(){
return gulp.src([
    
    'dist/templates/furnishing-template2/assets/css/bootstrap.min.css',
    'dist/templates/furnishing-template2/assets/css/font-awesome.min.css',
    'dist/templates/furnishing-template2/assets/css/plugins.css', 
    'dist/templates/furnishing-template2/assets/css/responsive.css',       
    'dist/templates/furnishing-template2/assets/css/toastr.min.css',
    'dist/templates/furnishing-template2/assets/css/pe-icon-7-stroke.css',
    'dist/templates/furnishing-template2/assets/css/style.css',
])
.pipe(minifyCSS())
.pipe(concat('ff.css'))
.pipe(gulp.dest(DIST_CSS));
});

gulp.task('fonts', function(){
return gulp.src(['dist/templates/furnishing-template2/assets/fonts/**/*'])
.pipe(gulp.dest(DIST_FONTS));
});

gulp.task('img', function(){
return gulp.src('dist/templates/furnishing-template2/assets/img/**/*')
.pipe(imagemin([
    imagemin.jpegtran({progressive: true}),
    imagemin.optipng({optimizationLevel: 5}),
    imagemin.svgo({plugins: [{removeViewBox: true}]})
])).pipe(gulp.dest(DIST_IMG));
});

gulp.task('favicon', function(){
return gulp.src('dist/templates/furnishing-template2/assets/favicon/**/*')
.pipe(imagemin())
.pipe(gulp.dest(DIST_FAVICON));
});

gulp.task('robots', function(){
return gulp.src(['dist/templates/furnishing-template2/robots.txt'])
.pipe(gulp.dest(DIST));
});

gulp.task('sitemap', function(){
return gulp.src(['dist/templates/furnishing-template2/sitemap.xml'])
.pipe(gulp.dest(DIST));
});
    