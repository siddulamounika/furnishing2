var express = require('express'),
  	partials = require('express-partials'),
  	app = express(),
  	routes = require('./routes'),
    errorHandlers = require('./middleware/errorHandlers'),
    env = require('./env'),
    config = require('./config/' + env.name),
    minifyHTML = require('express-minify-html'),
    compression = require('compression')
	;

  app.set('view engine', 'ejs');
  app.set('view options', {defaultLayout: 'layout'});

  app.use(partials());
  app.use(express.static(__dirname + '/dist'));

  app.use(minifyHTML({
    override:      true,
    exception_url: false,
    htmlMinifier: {
        removeComments:            true,
        collapseWhitespace:        true,
        collapseBooleanAttributes: true,
        removeAttributeQuotes:     true,
        removeEmptyAttributes:     true,
        minifyJS:                  true
    }
  }));
  app.use(compression());
  //routes
 // app.get('/', routes.uc);
  
  app.get('/', routes.index);
  app.get('/contact', routes.contact);
  app.get('/blog', routes.blog);
  app.get('/blog/:uname', routes.blogDetail);
  app.get('/about', routes.about);
  app.get('/service', routes.service);
  app.get('/privacy', routes.privacy);

	app.get('/products',routes.product);
  app.get('/product/curtain-shop-in-bangalore',routes.curtains);
  app.get('/product/stanley-sofa-shop-in-bangalore',routes.ssofa);
  app.get('/product/customised-sofa-shop-in-bangalore',routes.csofa);
  app.get('/product/mattress-shop-in-bangalore',routes.mattress);
  app.get('/product/bed-linen-shop-in-bangalore', routes.bedLinen);
  app.get('/product/door-mat-shop-in-bangalore', routes.doorMats);
  app.get('/product/blind-shop-in-bangalore', routes.blinds);
  app.get('/product/wallpaper-shop-in-bangalore',routes.wallPaper);
  app.get('/product/wood-flooring-shop-in-bangalore', routes.woodFlooring);
  app.get('/product/upholstery-shop-in-bangalore', routes.upholstery);
  app.get('/product/carpet-shop-in-bangalore', routes.carpets);
  app.get('/product/draping-rods-shop-in-bangalore', routes.drapingRods);
  app.get('/product/zinetgo-on-wheel-in-bangalore',routes.onWheel);
  app.get('/product/interior-design-in-bangalore',routes.interior);
  app.get('/product/turnkey-project-in-bangalore',routes.turnkey);

  // app.get('/product/:product',routes.productLocation);


  app.get('/error', function(req, res, next){
   	next(new Error('A contrived error'));
  });
  app.use(errorHandlers.error);
  app.use(errorHandlers.notFound);

  app.listen(config.port, ()=> console.log(`App server running on port `+ config.port));
